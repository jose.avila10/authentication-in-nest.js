import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './modules/users/users.module';
import { AuthService } from './modules/auth/service/auth.service';
import { AuthModule } from './modules/auth/auth.module';
import { JwtModule, JwtService } from '@nestjs/jwt';

@Module({
  imports: [UsersModule,
    AuthModule,
    JwtModule.register({
      secret: "здравствуйте групо нерио",
      signOptions: { expiresIn: "60s" },
    })],
  controllers: [AppController],
  providers: [AppService, AuthService],
})
export class AppModule { }
