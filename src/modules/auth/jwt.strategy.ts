import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor() {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: "здравствуйте групо нерио"
        })
    }

    async validate(payload: any) {
        //const user = await this.userService.getById(payload.id) // it can b to retrieve all info of the user
        return {
            id: payload.sub,
            name: payload.name,
            // ...user //This way the data of the user is also returned
        };
    }
}