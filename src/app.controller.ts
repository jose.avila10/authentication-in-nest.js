import { Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthenticatedGuard } from './modules/auth/authenticated.guard';
import { JwtAuthGuard } from './modules/auth/jwt-auth.guard';
import { localAuthGuard } from './modules/auth/local-auth.guard';
import { AuthService } from './modules/auth/service/auth.service';

@Controller()
export class AppController {
  constructor(private readonly authService: AuthService) { }

  @UseGuards(localAuthGuard)
  @Post('/login')
  login(@Request() req): any {
    return this.authService.login(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/protected')
  getHello(@Request() req): string {
    return req.user;
  }
}
