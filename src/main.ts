import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as passport from 'passport';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  //key = здравствуйте групо нерио

  const cors = require('cors');
  app.use(cors());

  await app.listen(3001);
}
bootstrap();
